package stats

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math"
	"os"
	"os/signal"
	"sort"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

// MessageStat contains rwLock and a map of chats
type MessageStat struct {
	Chat   map[int64]SingleChat
	rwLock sync.RWMutex
}

// SingleChat contains info about a single chat
type SingleChat struct {
	User         map[int]SingleUser
	FirstMessage time.Time
}

// SingleUser contains information about a single user
type SingleUser struct {
	Message  uint64
	Date     time.Time
	Username string
}

// MessageData contains the entire statistics
var MessageData MessageStat

// Set data based on SingleUser data
func (s *MessageStat) Set(chat int64, user int, username string, val SingleUser) {
	s.rwLock.Lock()
	defer s.rwLock.Unlock()
	s.Chat[chat].User[user] = val
}

// Increase message count of user and also update date and username
func (s *MessageStat) Increase(chat int64, user int, username string) {
	s.rwLock.Lock()
	defer s.rwLock.Unlock()

	// Check if parent element exist and create it if not
	if _, ok := s.Chat[chat]; !ok {
		s.Chat[chat] = SingleChat{
			User: map[int]SingleUser{
				user: SingleUser{
					Message:  1,
					Date:     time.Now(),
					Username: username,
				},
			},
			FirstMessage: time.Now(),
		}
	} else {
		s.Chat[chat].User[user] = SingleUser{
			Message:  s.Chat[chat].User[user].Message + 1,
			Date:     time.Now(),
			Username: username,
		}
	}
}

// GetChat gets the entire chat dataset
func (s *MessageStat) GetChat(chat int64) (SingleChat, bool) {
	s.rwLock.RLock()
	defer s.rwLock.RUnlock()
	val, found := s.Chat[chat]
	return val, found
}

// GetAll gets the entire chat dataset
func (s *MessageStat) GetAll() map[int64]SingleChat {
	s.rwLock.RLock()
	defer s.rwLock.RUnlock()
	data := s.Chat
	return data
}

func init() {
	// Initize empty object
	MessageData = MessageStat{
		Chat: map[int64]SingleChat{},
	}

	// Load stats from disk to memory
	statsFile, err := ioutil.ReadFile(`/var/lib/utelebot/stats.json`)
	if err != nil {
		log.Println(err)
	} else {
		if err := json.Unmarshal(statsFile, &MessageData.Chat); err != nil {
			log.Println(err)
		}
	}

	// Save data on program exit
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		WriteData()
	}()

	// Periodic save of data to disk
	go func() {
		for {
			time.Sleep(time.Minute * 5)
			WriteData()
		}
	}()
}

// WriteData writes the current data to stats.json
func WriteData() {
	stats, _ := json.MarshalIndent(MessageData.GetAll(), "", "	")
	err := os.WriteFile(`/var/lib/utelebot/stats.json`, stats, 0644)
	if err != nil {
		log.Println(err)
	}
}

// A data structure to hold key/value pairs
type pair struct {
	Key   int
	Value uint64
}

// A slice of pairs that implements sort.Interface to sort by values
type pairList []pair

func (p pairList) Len() int           { return len(p) }
func (p pairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p pairList) Less(i, j int) bool { return p[i].Value < p[j].Value }

// GetStringResponse fetches gets the toplist of chat users in the chat
// https://github.com/Thomas2500/uTeleBot/blob/master/commands/stats.php
func GetStringResponse(chatID int64) []string {
	go WriteData()

	text := "*CHAT TOPLIST*\n"

	// create list ordered by messages
	chatData, ok := MessageData.GetChat(chatID)
	if !ok {
		return []string{"No valid statistics found"}
	}

	chatPairList := make(pairList, len(chatData.User))

	i := 0
	for k, v := range chatData.User {
		chatPairList[i] = pair{k, v.Message}
		i++
	}

	sort.Sort(sort.Reverse(chatPairList))

	summaryMessagesTotal := uint64(0)
	summaryActiveLastHour := 0
	summaryActiveLastDay := 0

	index := 1
	for _, k := range chatPairList {
		text += "*#" + strconv.Itoa(index) + "* " + stringEscape(chatData.User[k.Key].Username) + ": " + strconv.FormatUint(chatData.User[k.Key].Message, 10) + "\n"
		index++
		summaryMessagesTotal += chatData.User[k.Key].Message

		if time.Since(chatData.User[k.Key].Date) <= time.Hour {
			summaryActiveLastHour++
			summaryActiveLastDay++
		} else if time.Since(chatData.User[k.Key].Date) <= time.Hour*24 {
			summaryActiveLastDay++
		}
	}

	// Calculate average messages per day
	days := time.Since(chatData.FirstMessage).Hours() / 24
	averageMessages := summaryMessagesTotal / uint64(math.Ceil(days))

	return []string{text, "*Summary*\nStats since " + chatData.FirstMessage.Format("02.01.2006 15:04:05") + "\n" + strconv.FormatUint(summaryMessagesTotal, 10) + " messages sent\n" + strconv.Itoa(summaryActiveLastHour) + " users active within the last hour\n" + strconv.Itoa(summaryActiveLastDay) + " users active within the last 24 hours\nOn average " + strconv.FormatUint(averageMessages, 10) + " messages per day"}
}

// AddRecord increases the message count
func AddRecord(update tgbotapi.Update) {
	username := update.Message.From.UserName
	if len(username) == 0 {
		username = strings.TrimSpace(update.Message.From.FirstName + " " + update.Message.From.LastName)
	}
	MessageData.Increase(update.Message.Chat.ID, update.Message.From.ID, username)
}

// GetHelp returns the help text of the command
func GetHelp() string {
	return "*Statistics*\nGet statistics of the current chat with `/stats`.\n"
}

func stringEscape(text string) string {
	text = strings.Replace(text, "_", "\\_", -1)
	text = strings.Replace(text, "*", "\\*", -1)
	text = strings.Replace(text, "[", "\\[", -1)
	text = strings.Replace(text, "`", "\\`", -1)
	text = strings.Replace(text, "#", "\\#", -1)
	text = strings.Replace(text, "@", "\\@", -1)
	return text
}
