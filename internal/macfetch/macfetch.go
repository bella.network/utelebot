package macfetch

import (
	"io"
	"net/http"
	"strings"

	"gitlab.com/bella.network/utelebot/pkg/securetls"
)

var client *http.Client

func init() {
	// Get TLS config
	client = securetls.GetClient()
}

// GetStringResponse fetches the vendor data of a given MAC address
func GetStringResponse(mac string) string {
	// Add mac padding to extend short mac to long version
	if len(mac) <= 12 {
		for i := len(mac); i <= 12; i++ {
			mac += "0"
		}
	}

	// Request lookup
	req, _ := http.NewRequest("GET", "https://api.macvendors.com/"+mac, nil)
	response, err := client.Do(req)
	if err != nil {
		return err.Error()
	}
	defer response.Body.Close()

	// Read content into variable
	// Limit response size to 1 MB
	content, err := io.ReadAll(io.LimitReader(response.Body, 1<<20))
	if err != nil {
		return err.Error()
	}

	// Send response to client
	return strings.TrimSpace(string(content))
}

// GetHelp returns the help text of the command
func GetHelp() string {
	return "*MAC Vendor Lookup*\nOutputs the vendor of a MAC address.\nAccepts long, short and extreme-short MAC addresses.\n`/mac 00:20:91:AB:CD:EF` or `/mac 002091ABCDEF` or `/mac 002091`"
}
