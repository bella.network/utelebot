package pingcomm

import (
	"bytes"
	"net"
	"os/exec"

	govalidator "github.com/asaskevich/govalidator"
)

var privateIPBlocks []*net.IPNet

func init() {
	for _, cidr := range []string{
		"127.0.0.0/8",    // IPv4 loopback
		"10.0.0.0/8",     // RFC1918
		"172.16.0.0/12",  // RFC1918
		"192.168.0.0/16", // RFC1918
		"::1/128",        // IPv6 loopback
		"fe80::/10",      // IPv6 link-local
		"fd00::/8",       // IPv6 private address space
	} {
		_, block, _ := net.ParseCIDR(cidr)
		privateIPBlocks = append(privateIPBlocks, block)
	}
}

// GetHelp returns the help text of the command
func GetHelp() string {
	return "*Ping*\nPerform a ping to a target like your domain, your public IP or to check if your server is reachable.\n*Examples:*\n`/ping example.com`\n`/ping 1.1.1.1`\n`/ping 2a03:4000:20:16::1`\nAttention: This program uses the local system resolver and does cache the DNS response."
}

// GetStringResponse fetches the vendor data of a given MAC address
func GetStringResponse(target string) string {
	// Determine sort of given target
	if govalidator.IsIPv4(target) {
		// We have an easteregg for you
		if target == "127.0.0.1" {
			return "*There is no place like 127.0.0.1*"
		} else if isPrivateIP(net.ParseIP(target)) {
			return "*Not allowed*\nYou have no permission to ping a private IP address."
		} else {
			cmd := exec.Command("sh", "-c", "ping -c 3 -i 0.2 -W1 -n "+target)
			var out bytes.Buffer
			cmd.Stdout = &out
			cmd.Stderr = &out
			cmd.Run()
			return "*Ping to " + target + "*\n\n```\n" + out.String() + "\n```"
		}

	} else if govalidator.IsIPv6(target) {
		cmd := exec.Command("sh", "-c", "ping6 -c 3 -i 0.2 "+target)
		var out bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &out
		cmd.Run()
		return "*Ping6 to " + target + "*\n\n```\n" + out.String() + "\n```"

	} else if govalidator.IsDNSName(target) {
		cmd := exec.Command("sh", "-c", "ping -c 3 -i 0.2 -W1 -n "+target)
		var out bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &out
		cmd.Run()
		return "*Domain ping to " + target + "*\n\n```\n" + out.String() + "\n```"

	}
	return "*Target is not valid*\nPlease provide a valid IPv4, IPv6 or domain name."
}

func isPrivateIP(ip net.IP) bool {
	for _, block := range privateIPBlocks {
		if block.Contains(ip) {
			return true
		}
	}
	return false
}
