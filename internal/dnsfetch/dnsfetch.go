package dnsfetch

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/bella.network/utelebot/pkg/securetls"
)

// DNSrecordValueToName contains a list of supported DNS records
var DNSrecordValueToName = map[int]string{
	1:  "A",
	2:  "NS",
	5:  "CNAME",
	6:  "SOA",
	15: "MX",
	16: "TXT",
	28: "AAAA",
	33: "SRV",
	44: "SSHFP",
}

// contains a dynamically built list of supported records for simple comparision
var recordLookup = []string{}

// DNSresponse contains data of a dns response from cloudflare
type DNSresponse struct {
	Status               int  `json:"Status"` // Response status
	Truncated            bool `json:"TC"`     // DNS answer was larder than a single UDP or TCP packet
	RecursiveDesired     bool `json:"RD"`     // Always true
	RecursionAvailable   bool `json:"RA"`     // Always true
	AnswerDNSSECVerified bool `json:"AD"`     // Every record was verified with DNSSEC
	ClientDNSSECDisabled bool `json:"CD"`     // Client disabled DNSSEC validation
	Question             []struct {
		Name string `json:"name"`
		Type int    `json:"type"`
	} `json:"Question"`
	Answer []struct {
		Name string `json:"name"`
		Type int    `json:"type"`
		TTL  int    `json:"TTL"`
		Data string `json:"data"`
	} `json:"Answer"`
}

var client *http.Client

func init() {
	// Get TLS config
	client = securetls.GetClient()

	// Build record lookup table
	for _, value := range DNSrecordValueToName {
		recordLookup = append(recordLookup, value)
	}
}

// IsRecordType checks if given string is a record type
func IsRecordType(data string) bool {
	return contains(recordLookup, data)
}

// GetStringResponse fetches DNS from CF and returnes it as formatted string
func GetStringResponse(target string, record string) string {
	start := time.Now()
	record = strings.ToUpper(record)
	data, err := fetch(target, record)
	if err != nil {
		return err.Error()
	}

	responseText := "Question: *" + data.Question[0].Name + "* IN *" + record + "*:\n"
	for _, value := range data.Answer {
		// Translate type to clear name
		recordValueToName, err := DNSrecordValueToName[value.Type]
		if !err {
			recordValueToName = strconv.Itoa(value.Type)
		}

		// Print response
		responseText += value.Name + " " + strconv.Itoa(value.TTL) + " IN " + recordValueToName + " " + value.Data + "\n"
	}

	responseText += "\nQuery time: " + strconv.FormatFloat(float64(time.Since(start)/time.Millisecond), 'f', 2, 64) + "ms"
	return responseText
}

// GetHelp returns the help text of the command
func GetHelp() string {
	help := "*DNS Query Command*\nThe command `/dns example.com` requests a basic `A` lookup to the given domain. The command can be extended with a record type like `/dns example.com AAAA`\n\n*Allowed records:*\n"
	i := 0
	for _, value := range DNSrecordValueToName {
		if i != 0 {
			help += ", "
		}
		help += value
		i++
	}
	return help
}

// fetch fetches current DNS data from Cloudflare DoH
func fetch(target string, record string) (DNSresponse, error) {
	// Chgeck if DNS type exists
	if !contains(recordLookup, record) {
		return DNSresponse{}, errors.New("Unsupported record type")
	}

	// Request lookup on cloudflare
	req, _ := http.NewRequest("GET", "https://cloudflare-dns.com/dns-query?name="+target+"&type="+record, nil)
	req.Header.Set("Accept", "application/dns-json")
	response, err := client.Do(req)
	if err != nil {
		return DNSresponse{}, err
	}
	defer response.Body.Close()

	// Read content CF DoH response into variable
	// Limit file size to 2 MB
	content, err := io.ReadAll(io.LimitReader(response.Body, 2<<20))
	if err != nil {
		return DNSresponse{}, err
	}

	// Unmarshal response data
	responseData := DNSresponse{}
	err = json.Unmarshal(content, &responseData)
	if err != nil {
		return DNSresponse{}, err
	}

	// return data
	return responseData, nil
}

func contains(sourceArray []string, search string) bool {
	for _, part := range sourceArray {
		if part == search {
			return true
		}
	}
	return false
}
