package traceroutecomm

import (
	"net"
	"os/exec"

	govalidator "github.com/asaskevich/govalidator"
)

var privateIPBlocks []*net.IPNet

func init() {
	for _, cidr := range []string{
		"127.0.0.0/8",    // IPv4 loopback
		"10.0.0.0/8",     // RFC1918
		"172.16.0.0/12",  // RFC1918
		"192.168.0.0/16", // RFC1918
		"::1/128",        // IPv6 loopback
		"fe80::/10",      // IPv6 link-local
		"fd00::/8",       // IPv6 private address space
	} {
		_, block, _ := net.ParseCIDR(cidr)
		privateIPBlocks = append(privateIPBlocks, block)
	}
}

// GetHelp returns the help text of the command
func GetHelp() string {
	return "*Traceroute*\nPerform a traceroute to a target like your domain, your public IP or to check if your server is reachable.\n*Examples:*\n`/traceroute example.com`\n`/traceroute 1.1.1.1`\nAttention: This program uses the local system resolver and does cache the DNS response."
}

// GetStringResponse fetches the vendor data of a given MAC address
func GetStringResponse(target string) string {
	// Determine sort of given target
	if govalidator.IsIPv4(target) {
		// We have an easteregg for you
		if target == "127.0.0.1" {
			return "*There is no place like 127.0.0.1*"
		} else if isPrivateIP(net.ParseIP(target)) {
			return "*Not allowed*\nYou have no permission to ping a private IP address."
		} else {
			out, err := exec.Command("sh", "-c", "traceroute -n -w 3 -q 2 -N 32 "+target).Output()
			if err != nil {
				return err.Error()
			}
			return "*Traceroute to " + target + "*\n\n```\n" + string(out) + "\n```"
		}

	} else if govalidator.IsIPv6(target) {
		out, err := exec.Command("sh", "-c", "traceroute6 -n -w 3 -q 2 -N 32 "+target).Output()
		if err != nil {
			return err.Error()
		}
		return "*Traceroute6 to " + target + "*\n\n```\n" + string(out) + "\n```"

	} else if govalidator.IsDNSName(target) {
		out, err := exec.Command("sh", "-c", "traceroute -n -w 3 -q 2 -N 32 "+target).Output()
		if err != nil {
			return err.Error()
		}
		return "*Domain traceroute to " + target + "*\n\n```\n" + string(out) + "\n```"

	}
	return "*Target is not valid*\nPlease provide a valid IPv4, IPv6 or domain name."
}

func isPrivateIP(ip net.IP) bool {
	for _, block := range privateIPBlocks {
		if block.Contains(ip) {
			return true
		}
	}
	return false
}
