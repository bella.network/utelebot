# uTeleBot

uTeleBot is a bot written in Go which provides some useful tools for IT people.

## TODO

- Don't depend on MAC address API
  - Download fom http://standards-oui.ieee.org/oui.txt, parse and update periodically
- Migrate all commands from https://github.com/Thomas2500/uTeleBot/tree/master/commands
