package main

type mainConfig struct {
	TelegramBotAPIKey string
	WebhookTarget     string
	Listen            string
}
