package main

import (
	"encoding/json"
	"log"
	"strconv"
	"strings"

	"gitlab.com/bella.network/utelebot/internal/dnsfetch"
	"gitlab.com/bella.network/utelebot/internal/macfetch"
	"gitlab.com/bella.network/utelebot/internal/pingcomm"
	"gitlab.com/bella.network/utelebot/internal/sshscan"
	"gitlab.com/bella.network/utelebot/internal/stats"
	"gitlab.com/bella.network/utelebot/internal/traceroutecomm"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func processMessage(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	// check if message was adressed to bot
	if update.Message.IsCommand() {
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")
		msg.ParseMode = "Markdown"

		// Get list of arguments split by space
		args := strings.Split(strings.TrimSpace(update.Message.CommandArguments()), " ")

		switch strings.ToLower(update.Message.Command()) {
		case "help", "bot", "man":
			// On help disable notification and webpage preview to not annoy users
			msg.DisableNotification = true
			msg.DisableWebPagePreview = true
			msg.Text = commandHelp()

		case "dns":
			if len(args) == 0 || len(args[0]) == 0 {
				msg.Text = dnsfetch.GetHelp()
			} else if len(args) == 1 {
				msg.Text = dnsfetch.GetStringResponse(string(args[0]), "A")
			} else {
				// Detect if first place is a record type and reorder arguments if needed
				if dnsfetch.IsRecordType(string(args[0])) {
					msg.Text = dnsfetch.GetStringResponse(string(args[1]), string(args[0]))
				} else {
					msg.Text = dnsfetch.GetStringResponse(string(args[0]), string(args[1]))
				}
			}

		case "mac":
			if len(args) == 0 || len(args[0]) == 0 {
				msg.Text = macfetch.GetHelp()
			} else {
				msg.Text = macfetch.GetStringResponse(args[0])
			}

		case "ping":
			if len(args) == 0 || len(args[0]) == 0 {
				msg.Text = pingcomm.GetHelp()
			} else {
				msg.Text = pingcomm.GetStringResponse(args[0])
			}

		case "traceroute":
			if len(args) == 0 || len(args[0]) == 0 {
				msg.Text = traceroutecomm.GetHelp()
			} else {
				go bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Performing traceroute. This will take a few moments ..."))
				msg.Text = traceroutecomm.GetStringResponse(args[0])
				msg.ReplyToMessageID = update.Message.MessageID
			}

		case "whoami":
			name := update.Message.From.UserName
			if name == "" {
				name = update.Message.From.FirstName
			}
			msg.Text = "*Hello " + name + ".*\nYou have the user ID " + strconv.Itoa(update.Message.From.ID) + ".\n"
			msg.Text += "This chat type is *" + update.Message.Chat.Type + "*."

		case "stats":
			stats := stats.GetStringResponse(update.Message.Chat.ID)
			for _, stat := range stats {
				msg.Text = stat
				bot.Send(msg)
			}
			return

		case "sshscan", "scanssh":
			if len(args) == 0 || len(args[0]) == 0 {
				msg.Text = sshscan.GetHelp()
			} else if len(args) == 1 {
				go bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Performing sshscan. This will take a few moments ..."))
				msg.Text = sshscan.GetStringResponse(args[0], 22)
				msg.ReplyToMessageID = update.Message.MessageID
			} else {
				port, err := strconv.ParseUint(args[1], 10, 16)
				if err != nil {
					msg.Text = "*ERROR* Invalid port given\n" + err.Error()
					break
				}
				go bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Performing sshscan. This will take a few moments ..."))
				msg.Text = sshscan.GetStringResponse(args[0], uint16(port))
				msg.ReplyToMessageID = update.Message.MessageID
			}

		default:
			// We don't know this command - skip it
			return
		}
		// Try to send message. If it fails print response
		if _, err := bot.Send(msg); err != nil {
			// Convert message which should be sent to json for better output
			jr, _ := json.Marshal(msg)
			log.Println(err, string(jr))
			// Send message that something went wrong
			go bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Sorry!\nSomething went really wrong at this end and a bug report was created. :-/"))
		}

		// Log executed command
		go func(update tgbotapi.Update) {
			log.Println("Executed /" + strings.ToLower(update.Message.Command()) + " " + update.Message.CommandArguments() + " for User " + update.Message.From.UserName)
		}(update)

	} else {
		stats.AddRecord(update)
	}
}
