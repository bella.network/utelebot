package main

var availableCommands = map[string]string{
	"dns":        "Perform a DNS lookup",
	"mac":        "Get the vendor of a MAC address",
	"ping":       "Perform a ping on a IPv4/IPv6 or domain",
	"sshscan":    "SSH security scan. Use only with permission",
	"stats":      "We all love statistics",
	"traceroute": "Perform a traceroute",
	"whoami":     "Who am I?",
}

func commandHelp() string {
	help := `Hello!
My name is *uTeleBot* (Version ` + programVersion + `+` + commitHash + `) and I was created by [Thomas2500](http://t.me/Thomas2500).

Available commands:
`

	for name, desc := range availableCommands {
		help += "*" + name + "*: " + desc + "\n"
	}

	help += `
You can grab my source code from https://gitlab.com/bella.network/utelebot
`
	return help
}
