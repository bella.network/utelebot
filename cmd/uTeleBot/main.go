package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var (
	commitHash     = "00000000"
	programVersion = "20190110142100"
	buildDate      = "2019-01-10"
)

func main() {
	// Read configuration from config file
	var config mainConfig
	// Load stats from disk to memory
	configFile, err := os.ReadFile(`/etc/utelebot/config.json`)
	if err != nil {
		log.Println(err)
		if configFile, err = os.ReadFile(`config.json`); err != nil {
			log.Println(err)
			return
		}
	}
	if err := json.Unmarshal(configFile, &config); err != nil {
		log.Println(err)
		return
	}

	// Check if config is valid
	if config.WebhookTarget == "https://example.com/" {
		log.Fatal("Please configure this bot in /etc/utelebot/config.json")
	}

	// Set API key and set custom target address
	bot, err := tgbotapi.NewBotAPI(config.TelegramBotAPIKey)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)

	// Set webhook target on program start
	_, err = bot.SetWebhook(tgbotapi.NewWebhook(config.WebhookTarget))
	if err != nil {
		log.Fatal(err)
	}
	// Verify webhook info and check if calls failed
	info, err := bot.GetWebhookInfo()
	if err != nil {
		log.Fatal(err)
	}
	if info.LastErrorDate != 0 {
		log.Printf("Telegram callback failed: %s", info.LastErrorMessage)
		//bot.Send(tgbotapi.NewMessage(79785025, info.LastErrorMessage))
	}

	// Start http server and listen for incoming data
	updates := bot.ListenForWebhook("/")
	go func() {
		err := http.ListenAndServe(config.Listen, nil)
		if err != nil {
			log.Fatal(err)
		}
	}()

	// Exit program after a few seconds
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		time.Sleep(time.Second * 5)
		os.Exit(0)
	}()

	// Process incoming messages
	for update := range updates {
		// Incoming message was nil - something wrong? - Log it
		if update.Message == nil {
			ms, _ := json.Marshal(update)
			log.Println(string(ms))
			continue
		}

		// Async start processing message
		go processMessage(bot, update)
	}
}
