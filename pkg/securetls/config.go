package securetls

import (
	"crypto/tls"
	"net/http"
	"time"
)

// GetClient returns a preconfigured HTTP client
func GetClient() *http.Client {
	return &http.Client{
		Timeout: time.Second * 30,
		Transport: &http.Transport{
			MaxIdleConnsPerHost: 10,
			TLSHandshakeTimeout: time.Second * 5,
			TLSClientConfig: &tls.Config{
				// Require a minimum of TLSv1.2
				MinVersion: tls.VersionTLS12,
				// Use strongest curves first
				CurvePreferences: []tls.CurveID{tls.X25519, tls.CurveP521, tls.CurveP384, tls.CurveP256},
				// Sensitive data could be transferred
				// Only allow very strong ciphers
				CipherSuites: []uint16{
					tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
					tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
					tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
					tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
				},
			},
		},
	}
}
