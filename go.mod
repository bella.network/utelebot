module gitlab.com/bella.network/utelebot

go 1.23

require (
	github.com/asaskevich/govalidator v0.0.0-20230301143203-a9d515a09cc2
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
)

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
